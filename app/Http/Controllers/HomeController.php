<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

/**
 * Class HomeController
 *
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Display home index page.
     *
     * @return View
     */
    public function index(): View
    {
        return view('home');
    }
}
