<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Services\User\StoreService;
use Illuminate\Http\RedirectResponse;

/**
 * Class UserController
 *
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * Stores a new user.
     *
     * @param StoreUserRequest $request
     * @param StoreService     $service
     *
     * @return RedirectResponse
     */
    public function store(StoreUserRequest $request, StoreService $service): RedirectResponse
    {
        if ($user = $service->store($request->all())) {
            return back()->with('success', "A user with email {$user->email} was successfully created.");
        }

        return back()->with('danger', "User was not created");
    }
}
