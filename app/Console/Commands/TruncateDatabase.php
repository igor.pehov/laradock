<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class TruncateDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:truncate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Trucate database.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Eloquent::unguard();

        // Truncate all tables, except migrations
        $tables = \DB::select('SHOW TABLES');
        $dbName = \Config::get('database.connections.mysql.database');
        $this->info('Preparing to drop all tables in '.$dbName.' database');
        \DB::statement("SET foreign_key_checks = 0");
        $tableName = 'Tables_in_'.$dbName;
        foreach ($tables as $table) {
            \Schema::drop($table->{$tableName});
            $this->info('Table '.$table->{$tableName}.' dropped');
        }
        $this->info('Database '.$dbName.' successfully truncated!');
        \DB::statement("SET foreign_key_checks = 1");
    }
}
