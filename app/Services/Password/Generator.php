<?php

namespace App\Services\Password;

/**
 * Class Generator
 *
 * @package App\Services\Password
 */
class Generator
{
    /**
     * Generate random password.
     *
     * @param int $length
     *
     * @return string
     */
    public function generate(int $length = 12): string
    {
        return str_random($length);
    }
}
