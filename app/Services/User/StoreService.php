<?php

namespace App\Services\User;

use App\Services\Password\Generator;
use App\User;

/**
 * Class StoreService
 *
 * @package App\Services\User
 */
class StoreService
{
    /**
     * User model.
     *
     * @var User
     */
    private $model;

    /**
     * Password generator.
     *
     * @var Generator
     */
    private $passwordGenerator;

    /**
     * StoreService constructor.
     *
     * @param User      $model
     * @param Generator $passwordGenerator
     */
    public function __construct(User $model, Generator $passwordGenerator)
    {
        $this->model             = $model;
        $this->passwordGenerator = $passwordGenerator;
    }

    /**
     * Stores new user.
     *
     * @param array $data
     *
     * @return User|null
     */
    public function store(array $data): ?User
    {
        array_set($data, 'password', $this->passwordGenerator->generate());

        logger(123123);

        $this->model->fill($data);

        if ($this->model->save()) {
            return $this->model;
        }

        return null;
    }
}
