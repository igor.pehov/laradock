<?php

namespace Tests\Unit;

use App\Services\Password\Generator;
use Tests\TestCase;

class PasswordGeneratorTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGenerate(): void
    {
        $length    = 33;
        $generator = new Generator();

        $this->assertEquals($length, strlen($generator->generate($length)));
        $this->assertTrue(false);
    }
}
