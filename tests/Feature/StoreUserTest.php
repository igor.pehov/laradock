<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StoreUserTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp()
    {
        parent::setUp();

        $this->withoutMiddleware();
    }

    /**
     * Test case with success store.
     *
     * @dataProvider userDataProvider
     *
     * @param array $data
     */
    public function testStoreUserSuccess(array $data): void
    {
        // Make a store request.
        $response = $this->post(route('users.store'), $data);

        // Should be redirect response.
        $this->assertTrue($response->isRedirection());

        // Session should contain success message.
        $response->assertSessionHas('success');

        // Sessions should not contain error message.
        $response->assertSessionMissing('danger');
    }

    /**
     * Test validation fails.
     *
     * @param array $data
     *
     * @dataProvider userDataProvider
     */
    public function testStoreUserFailValidation(array $data): void
    {
        $data = [];

        // Make a store request.
        $response = $this->post(route('users.store'), $data);

        // Should be redirected.
        $this->assertTrue($response->isRedirection());

        // Sessions should contain validation errors.
        $response->assertSessionHasErrors(['name', 'email']);
    }

    /**
     * Password should be set automatically for new users.
     *
     * @param array $data
     *
     * @dataProvider  userDataProvider
     */
    public function testUserHasPasswordAfterCreation(array $data): void
    {
        // Make a store request.
        $response = $this->post(route('users.store'), $data);

        // Get a new user.
        $user = User::query()->where('email', $data['email'])->first();

        // Password should not be empty.
        $this->assertNotEmpty($user->password);
    }


    /**
     * User data provider.
     *
     * @return \Generator
     */
    public function userDataProvider(): \Generator
    {
        yield [
            [
                'name'  => 'Test',
                'email' => 'test_email@com.ua',
            ]
        ];
    }
}
