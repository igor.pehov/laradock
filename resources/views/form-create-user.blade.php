<form class="form-inline form-create-user" action="{{ route('users.store') }}" method="POST">
    <div class="form-group">
        <label for="email" class="control-label">Email address</label>
        <input type="email" class="form-control" id="email" name="email" />
    </div>
    <div class="form-group">
        <label for="name" class="control-label">Name</label>
        <input type="text" class="form-control" id="name" name="name">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    {{ csrf_field() }}
</form>
