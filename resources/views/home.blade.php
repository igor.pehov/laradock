@extends('layout')

@section('content')
    <div class="row jumbotron">
        <h1>Hello!</h1>
        <h3>Fill in the form below to create a user</h3>
    </div>

    @include('errors')
    @include('form-create-user')
@endsection
