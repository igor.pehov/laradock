@if (session()->has(['danger', 'success'], []))
    <div class="row" style="margin-top: 15px;">
        <div class="col-md-offset-4 col-md-4">
            @foreach (['danger', 'success'] as $types)
                @foreach ($types as $type)
                    <div class="alert alert-{{ $type }}">
                        {{ session()->get($type) }}
                    </div>
                @endforeach
            @endforeach
        </div>
    </div>
@endif
