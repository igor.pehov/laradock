@if ($errors->any())
    <div class="row" style="margin-top: 15px;">
        <div class="col-md-offset-4 col-md-4">
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger">
                    {{ $error }}
                </div>
            @endforeach
        </div>
    </div>
@endif
