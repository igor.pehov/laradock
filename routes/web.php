<?php

Route::get('/', 'HomeController@index')->name('home');

Route::group([
    'as'     => 'users.',
    'prefix' => 'users',
], function () {
   Route::post('store', 'UserController@store')->name('store');
});
